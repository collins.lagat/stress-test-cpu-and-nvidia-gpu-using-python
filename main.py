from psutil import sensors_temperatures
from datetime import datetime
import time
import subprocess
import os
import signal
from threading import Timer
from typing import NoReturn, Callable, List, Union, Dict, Optional
import numpy as np
import pandas as pd
from terminaltables import AsciiTable

HALF_MINUTE = 30
ONE_MINUTE = 60
TWO_MINUTES = 120
THIRTY_MINUTES = 1800
ONE_HOUR = 3600
TWO_HOURS = 7200
THREE_HOURS = 10800

RUNTIME = HALF_MINUTE

STATUS = "after"
CPU_CORES = 8
GPU_LOAD = 1


def get_core_temp(core_dict, core_number):
    return core_dict[core_number].current


def print_to_console(datum):
    table_data = [
        ["elapsed_time", "package", "core_0", "core_1", "core_2", "core_3", "discrete_gpu"],
        datum
    ]
    title = "Temperature @ {} seconds".format(int(datum[0]))
    table = AsciiTable(table_data, title=title)
    print(table.table)


class CPU:
    """
    Central Processing Unit
    """

    def __init__(self, number_of_cores: int, sensor: Callable[[], Union[Dict[str, list], dict]]):
        self.number_of_cores = number_of_cores
        self.sensor = sensor
        self._pid = None

    def run(self):
        stress = "stress -c {0}"
        stress_ng = "stress-ng --cpu {0} --cpu-method all"
        cmd = stress
        process = subprocess.Popen(cmd.format(self.number_of_cores).split(), preexec_fn=os.setpgrp)
        self._pid = process.pid

    def is_running(self) -> bool:
        return self._pid is not None

    def stop(self):
        if self.is_running():
            os.killpg(os.getpgid(self._pid), signal.SIGTERM)
            self._pid = None
        return 0

    def report_temp(self) -> List[int]:
        data = self.sensor()
        package = data["coretemp"]
        datum = [
            get_core_temp(package, 0),
            get_core_temp(package, 1),
            get_core_temp(package, 2),
            get_core_temp(package, 3),
            get_core_temp(package, 4)
        ]
        return datum


class GPU:
    """
    Graphics Processing Unit
    """

    def __init__(self, load: int):
        self._pid_list = None
        self.load = load

    @staticmethod
    def _run() -> int:
        cmd = "./GpuTest /test=fur /width=1920 /height=1080 /fullscreen"
        process = subprocess.Popen(cmd.format(os.getcwd()).split(),
                                   cwd="{}/GPU".format(os.getcwd()),
                                   preexec_fn=os.setpgrp)

        return process.pid

    def run(self):
        self._pid_list = []
        for i in range(self.load):
            self._pid_list.append(
                self._run()
            )

    def is_running(self) -> bool:
        return self._pid_list is not None

    def stop(self) -> int:
        if self.is_running():
            for pid in self._pid_list:
                self._stop(pid)
            self._pid_list = None
        return 0

    def _stop(self, pid: int) -> int:
        if self.is_running():
            os.killpg(os.getpgid(pid), signal.SIGTERM)
        return 0

    @staticmethod
    def report_temp() -> int:
        cmd = "nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader"
        process = subprocess.Popen(cmd.split(),
                                   stdin=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   )
        output, error = process.communicate()
        temperature: str = output.decode(encoding='UTF-8').strip()
        return int(temperature)


class TemperatureStore:
    def __init__(self):
        self._dataset: List[List[Union[int, float]]] = []

    def post_temperature(self, datum: List[Union[int, float]]):
        self._dataset.append(datum)

    def save_as_csv(self):
        dataset = np.array(self._dataset, dtype=np.int)
        columns = ["elapsed_time", "package", "core_0", "core_1", "core_2", "core_3", "discrete_gpu"]
        timestamp = datetime.today()
        filename = "data/{}-{}-{}{}{}-dataset.csv".format(STATUS,
                                                          timestamp.date(),
                                                          timestamp.time().hour,
                                                          timestamp.time().minute,
                                                          timestamp.time().second)
        pd.DataFrame(dataset, columns=columns).to_csv(filename, index=False)


class Clock:
    def __init__(self, interval: int):
        self.interval = interval
        self._ref_time = None
        self._current_interval_timer: Optional[Timer] = None
        self._runtime_timer: Optional[Timer] = None
        self._repeating_fun: Callable[[int], NoReturn] | None = None
        self._ending_fun: Callable[[int], NoReturn] | None = None

    def _notify(self):
        self._repeating_fun(self._get_time_interval())

    def _run(self):
        self._notify()
        self._start_timer()

    def _start_timer(self):
        self._current_interval_timer = Timer(self.interval, self._run)
        self._current_interval_timer.start()

    def _stop_timer(self):
        if self._current_interval_timer is not None:
            self._current_interval_timer.cancel()
        self._current_interval_timer = None

    def _start_stopwatch(self):
        self._ref_time = time.perf_counter()

    def _reset_stopwatch(self):
        self._ref_time = None

    def _get_time_interval(self) -> float:
        return time.perf_counter() - self._ref_time

    def start(self, runtime: int, repeating_fn: Callable[[float], NoReturn], ending_fn: Callable[[float], NoReturn]):
        self._repeating_fun = repeating_fn
        self._ending_fun = ending_fn
        self._start_timer()
        self._start_stopwatch()
        self._runtime_timer = Timer(runtime, self.stop)
        self._runtime_timer.start()

    def stop(self):
        if self._ending_fun is not None:
            self._ending_fun(self._get_time_interval())
        if self._runtime_timer is not None:
            self._runtime_timer.cancel()
        self._stop_timer()
        self._reset_stopwatch()
        self._repeating_fun = None
        self._ending_fun = None


class Job:
    def __init__(self, internal_clock: Clock, internal_store: TemperatureStore, cpu: CPU, gpu: GPU, runtime: int):
        self.cpu = cpu
        self.gpu = gpu
        self.runtime = runtime
        self.internal_clock = internal_clock
        self.internal_store = internal_store
        self.active = False

    def run(self):
        self.active = True
        self.cpu.run()
        self.gpu.run()
        self.internal_clock.start(runtime=self.runtime, repeating_fn=self.report_time, ending_fn=self.final_report)

    def stop(self):
        self.gpu.stop()
        self.cpu.stop()
        self.active = False

    def get_temp(self) -> List[int]:
        cpu_temp = self.cpu.report_temp()
        gpu_temp = self.gpu.report_temp()
        datum: List[int] = [*cpu_temp, gpu_temp]
        return datum

    def report_time(self, interval: float):
        datum: List[Union[int, float]] = [interval, *self.get_temp()]
        print_to_console(datum)
        self.internal_store.post_temperature(datum=datum)

    def final_report(self, interval: float):
        print("Called")
        self.report_time(interval=interval)
        self.stop()

    def generate_report(self):
        self.internal_store.save_as_csv()


def main():
    cpu = CPU(number_of_cores=CPU_CORES, sensor=sensors_temperatures)
    gpu = GPU(GPU_LOAD)
    clock = Clock(interval=5)
    store = TemperatureStore()
    job = Job(internal_clock=clock, internal_store=store, cpu=cpu, gpu=gpu, runtime=RUNTIME)
    try:
        job.run()
        time.sleep(RUNTIME)
    finally:
        job.generate_report()


if __name__ == '__main__':
    main()
